--## HECHO POR KACPER STANISLAW DUDA Y GEORGE MIHAI ALEXANDRU

DROP PROCEDURE IF EXISTS CrearFacturacion;
USE jardineria;

-- CREACION DE LAS TABLAS
DELIMITER |
CREATE PROCEDURE CrearFacturacion()
	BEGIN	
		CREATE TABLE IF NOT EXISTS Facturas (
			CodigoCliente INTEGER,
			BaseImponible NUMERIC(15,2),
			IVA NUMERIC(15,2),
			Total NUMERIC(15,2),
			CodigoFactura INTEGER,
			CONSTRAINT PK_Facturas PRIMARY KEY(CodigoFactura),
			CONSTRAINT FK_Facturas_Clientes FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente)
		);
		DESC Facturas;

		CREATE TABLE IF NOT EXISTS Comisiones (
			ID INTEGER AUTO_INCREMENT,
			CodigoEmpleado INTEGER,
			Comision NUMERIC(15,2),
			CodigoFactura INTEGER,
			CONSTRAINT PK_Comisiones PRIMARY KEY(ID),
			CONSTRAINT FK_Comisiones_Facturas FOREIGN KEY(CodigoFactura) REFERENCES Facturas(CodigoFactura),
			CONSTRAINT FK_Comisiones_Empleados FOREIGN KEY(CodigoEmpleado) REFERENCES Empleados(CodigoEmpleado)
		);
		DESC Comisiones;

		CREATE TABLE IF NOT EXISTS AsientoContable (
			ID INTEGER AUTO_INCREMENT,
			CodigoCliente INTEGER,
			Debe NUMERIC(15,2),
			Haber NUMERIC(15,2),
			CONSTRAINT PK_AsientoContable PRIMARY KEY(ID),
			CONSTRAINT FK_AsientoContable_Clientes FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente)
		);
		DESC AsientoContable;
	END;
|
DELIMITER ;
CALL CrearFacturacion();


-- ULTIMA FACTURA
DROP FUNCTION IF EXISTS UltimaFactura;

DELIMITER |
CREATE FUNCTION UltimaFactura()
	RETURNS INT
	BEGIN
		-- DECLARACION DE VARIABLES
		DECLARE Factura INT;
		
		-- ESTABLECER EL 'CodigoFactura' MAXIMO A LA VARIABLE 'Factura'
		SET Factura = (SELECT MAX(CodigoFactura) FROM Facturas);

		-- SI NO HAY 'CodigoFactura' MAXIMO DEVUELVE 1
		IF Factura IS NULL THEN
			SET Factura = 1;
		end IF;
		RETURN Factura;
	END
|
DELIMITER ;
SELECT UltimaFactura();


-- FACTURAR
DROP PROCEDURE IF EXISTS Facturar;
DELIMITER |
CREATE PROCEDURE Facturar()
	BEGIN
		-- DECLARACION DE VARIABLES
		DECLARE ClienteF INT;
		DECLARE PedidoF INT;
		DECLARE EmpleadoF INT;
		DECLARE ContadorClientes INT;
		DECLARE ContadorPedidos INT;
		DECLARE NumeroClientesTotales INT;
		DECLARE NumeroPedidosTotales INT;
		DECLARE NumeroPedidoMAX INT;
		
		DECLARE ComisionF INT;
		DECLARE TotalF INT;
		DECLARE BaseImponibleF INT;
	
		SET NumeroPedidosTotales = (SELECT COUNT(*) FROM Pedidos);
		SET NumeroPedidoMAX = (select MAX(CodigoPedido) FROM Pedidos);
		
		SET ContadorClientes = 1;
		SET ContadorPedidos = 1;
	
		-- BUCLE PARA SACAR LOS CLIENTES Y LOS PEDIDOS QUE HAY
		WHILE ContadorClientes <= NumeroPedidoMAX DO
			WHILE ContadorPedidos <= NumeroPedidoMAX DO
				-- SE ESTABLECEN CONSULTAS A VARIABLES PARA USARLAS MAS ADELANTE
				SET ClienteF = (SELECT CodigoCliente FROM Pedidos WHERE CodigoCliente = ContadorClientes AND CodigoPedido = ContadorPedidos);
				SET EmpleadoF = (SELECT CodigoEmpleadoRepVentas FROM Clientes WHERE CodigoCliente = ClienteF);
				SET PedidoF = (SELECT CodigoPedido FROM Pedidos WHERE CodigoCliente = ContadorClientes AND CodigoPedido = ContadorPedidos);
				SET TotalF = (SELECT SUM(Cantidad * PrecioUnidad) FROM DetallePedidos WHERE CodigoPedido = ContadorPedidos);
				SET BaseImponibleF = (SELECT SUM((Cantidad * PrecioUnidad) / 0.21) FROM DetallePedidos WHERE CodigoPedido = ContadorPedidos);
				SET ComisionF = (SELECT SUM((Cantidad * PrecioUnidad)*0.05) FROM DetallePedidos WHERE CodigoPedido = ContadorPedidos);
				
				-- SI LA VARIABLE 'TotalF' ESTA VACIA ENTONCES NO HACE NADA, PERO SI TIENE CONTENIDO EJECUTA LO SIGUIENTE
				IF TotalF IS NOT NULL THEN
					-- SI LA VARIABLE 'ClienteF' ESTA VACIA ENTONCES NO HACE NADA, PERO SI TIENE UN 'CodigoCliente' INSERTA LOS DATOS EN LA TABLA FACTURAS Y COMISIONES
					IF ClienteF IS NOT NULL THEN
						INSERT INTO Facturas(CodigoCliente, BaseImponible, IVA, Total, CodigoFactura) VALUES (ClienteF, BaseImponibleF, 0.21, TotalF, PedidoF);
						INSERT INTO Comisiones(CodigoEmpleado, Comision, CodigoFactura) VALUES (EmpleadoF, ComisionF, PedidoF);
					END IF;
				END IF;
					
				-- AUMENTO DE VARIABLE DE BUCLE
				SET ContadorPedidos = ContadorPedidos + 1;
			END WHILE;
			-- SI EL CONTADOR DE LOS PEDIDOS LLEGA A NUMERO DE PEDIDO MAX VUELVE A SER 1
			IF ContadorPedidos > NumeroPedidoMAX THEN
				SET ContadorPedidos = 1;
			END IF;
			SET ContadorClientes = ContadorClientes + 1;
		END WHILE;
	END;
|
DELIMITER ;
CALL Facturar();
SELECT CodigoFactura,CodigoCliente,IVA,BaseImponible,Total FROM Facturas;


-- GENERAR CONTABILIDAD
DROP PROCEDURE IF EXISTS GenerarContabilidad;
DELIMITER |
CREATE PROCEDURE GenerarContabilidad()
	BEGIN
		-- DECLARACION DE VARIABLES
		DECLARE ContadorClientes INT;
		DECLARE Cliente INT;
		DECLARE NumeroClientesTotales INT;		
		DECLARE ClienteDEBE NUMERIC(15,2);

		SET NumeroClientesTotales = (SELECT COUNT(*) FROM Clientes);
		SET ContadorClientes = 1;

		-- BUCLE PARA SACAR LOS CLIENTES
		WHILE ContadorClientes <= NumeroClientesTotales DO
			SET Cliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = ContadorClientes);
			SET ClienteDEBE = (SELECT SUM(Total) FROM Facturas WHERE CodigoCliente = ContadorClientes GROUP BY CodigoCliente);

			-- SI LA VARIABLE 'Cliente' ESTA VACIA ENTONCES NO HACE NADA, PERO SI TIENE UN 'CodigoCliente' EJECUTA LA SIGUIENTE CONDICION QUE COMPRUEBA QUE LA VARIABLE 'ClienteDEBE' NO ESTE VACIA E INSERTA LOS DATOS EN LA TABLA ASIENTOCONTABLE
			IF Cliente IS NOT NULL THEN
				IF ClienteDEBE IS NOT NULL THEN
					INSERT INTO AsientoContable(CodigoCliente, DEBE) VALUES (Cliente, ClienteDEBE);
				END IF;
			END IF;
			
			-- AUMENTO DE VARIABLE DE BUCLE
			SET ContadorClientes = ContadorClientes + 1;
		END WHILE;
	END;
|
DELIMITER ;
CALL GenerarContabilidad();
SELECT * FROM AsientoContable;


-- PARTE AÑADIDA
DROP PROCEDURE IF EXISTS CalculaPedidosClientes;
DELIMITER |
CREATE PROCEDURE CalculaPedidosClientes()
	BEGIN
		-- DECLARACION DE VARIABLES
		DECLARE ContadorClientes INT;
		DECLARE Cliente INT;
		DECLARE NumeroClientesTotales INT;		
		DECLARE ClienteTotalPedidos INT;
		DECLARE ClienteNombre VARCHAR(40);

		SET NumeroClientesTotales = (SELECT COUNT(*) FROM Clientes);
		SET ContadorClientes = 1;
		
		DROP TABLE IF EXISTS TotalPedidosRealizadosClientes;
		
		CREATE TABLE IF NOT EXISTS TotalPedidosRealizadosClientes(
			CodigoCliente INTEGER,
			NombreApellidos VARCHAR(40),
			TotalPedidos INT,
			CONSTRAINT FK_TotalPedidosRealizadosClientes_Clientes FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente)
		);
		DESC TotalPedidosRealizadosClientes;

		-- BUCLE PARA SACAR LOS CLIENTES
		WHILE ContadorClientes <= NumeroClientesTotales DO
			SET Cliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = ContadorClientes);
			SET ClienteTotalPedidos = (SELECT COUNT(CodigoPedido) FROM Pedidos WHERE CodigoCliente = Cliente ORDER BY CodigoCliente);
			SET ClienteNombre = (SELECT NombreCliente FROM Clientes WHERE CodigoCliente = ContadorClientes);

			IF Cliente IS NOT NULL THEN
				REPLACE INTO TotalPedidosRealizadosClientes VALUES(Cliente,ClienteNombre,ClienteTotalPedidos);
			END IF;
			
			-- AUMENTO DE VARIABLE DE BUCLE
			SET ContadorClientes = ContadorClientes + 1;
		END WHILE;
	END;
|
DELIMITER ;
CALL CalculaPedidosClientes();
SELECT * FROM TotalPedidosRealizadosClientes;