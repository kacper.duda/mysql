--## HECHO POR KACPER STANISLAW DUDA Y GEORGE MIHAI ALEXANDRU


DROP PROCEDURE IF EXISTS ActualizarLimiteCredito;

USE jardineria;

/* PROCEDIMIENTO ACTUALIZAR LIMITE DE CREDITO */
DELIMITER |
CREATE PROCEDURE ActualizarLimiteCredito()
	BEGIN
		DECLARE Cliente INT;
		DECLARE NumeroClientesTotales INT;		
		DECLARE ContadorClientes INT;
		DECLARE Porcentaje NUMERIC(15,2);
		DECLARE SacarAumentoCredito NUMERIC(15,2);	

		CREATE TABLE IF NOT EXISTS ActualizacionLimiteCredito (
			CodigoCliente INT,
			Fecha DATE,
			Incremento NUMERIC(15,2),
			CONSTRAINT PK_ActualizacionLimiteCredito PRIMARY KEY(CodigoCliente)
		);

		SET NumeroClientesTotales = (SELECT COUNT(*) FROM Clientes);
		SET ContadorClientes = 1;

		WHILE ContadorClientes <= NumeroClientesTotales DO
			SET Cliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = ContadorClientes);

			IF Cliente IS NOT NULL THEN
				SET Porcentaje = (SELECT SUM(DetallePedidos.PrecioUnidad * DetallePedidos.Cantidad)*0.15 AS PagoTotal FROM Clientes,Pedidos,DetallePedidos WHERE Clientes.CodigoCliente = ContadorClientes AND Pedidos.CodigoCliente = ContadorClientes AND Pedidos.CodigoPedido=DetallePedidos.CodigoPedido AND Pedidos.FechaPedido BETWEEN '2008-01-01' AND '2010-12-31' GROUP BY Clientes.CodigoCliente ORDER BY Clientes.CodigoCliente);
			END IF;

			IF Porcentaje IS NULL THEN
				SET Porcentaje = 0;
			END IF;
			
			IF Cliente IS NOT NULL THEN 
				REPLACE INTO ActualizacionLimiteCredito VALUES (Cliente,(SELECT CURDATE()),Porcentaje);
				SET SacarAumentoCredito = (SELECT Incremento FROM ActualizacionLimiteCredito WHERE CodigoCliente = ContadorClientes);
				UPDATE Clientes SET LimiteCredito = LimiteCredito + Porcentaje WHERE CodigoCliente = ContadorClientes;
			END IF;
			
			SET ContadorClientes = ContadorClientes + 1;
		END WHILE;
	END;
|
DELIMITER ;
CALL ActualizarLimiteCredito();