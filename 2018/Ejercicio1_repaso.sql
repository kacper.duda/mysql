/*------------------------------------*/
/*     CREACIÓN DE BASE DE DATOS      */
/*------------------------------------*/

DROP DATABASE IF EXISTS EJ1_repaso;

CREATE DATABASE EJ1_repaso;

USE EJ1_repaso;

CREATE TABLE CLIENTES (
    DNI CHAR(9) NOT NULL,
    Nombre VARCHAR(20),
    Apellidos VARCHAR(40),
    Telefono INT(9),
    Email VARCHAR(40) UNIQUE,
    CONSTRAINT PK_CLIENTES PRIMARY KEY(DNI)
);
DESC CLIENTES;

CREATE TABLE TIENDAS(
    Nombre VARCHAR(30),
    Provincia VARCHAR(40),
    Localidad VARCHAR(40),
    Direccion VARCHAR(60),
    Telefono CHAR(9),
    DiaApertura ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'),
    DiaCierre ENUM('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'),
    HoraApertura TINYINT UNSIGNED,
    HoraCierre TINYINT UNSIGNED,
    CONSTRAINT PK_TIENDAS PRIMARY KEY(Nombre)
);
DESC TIENDAS;

CREATE TABLE OPERADORAS(
    Nombre VARCHAR(20),
    ColorLogo ENUM('Rojo','Azul','Naranja','Morado'),
    PorcentajeCobertura TINYINT UNSIGNED COMMENT 'Unidades en porcentaje (%)',
    FrecuenciaGSM SMALLINT UNSIGNED,
    PaginaWEB VARCHAR(100),
    CONSTRAINT PK_OPERADORAS PRIMARY KEY(Nombre)
);
DESC OPERADORAS;

CREATE TABLE TARIFAS (
    Nombre VARCHAR(50),
    Nombre_OPERADORAS VARCHAR(20),
    TamanoDatos TINYINT UNSIGNED,
    TipoDatos ENUM('MB','GB','TB'),
    MinutosGratis SMALLINT UNSIGNED,
    SMSGratis ENUM('Si','No'),
    CONSTRAINT PK_TARIFAS PRIMARY KEY(Nombre),
    CONSTRAINT FK_TARIFAS_OPERADORAS FOREIGN KEY(Nombre_OPERADORAS) REFERENCES OPERADORAS(Nombre)
);
DESC TARIFAS;

CREATE TABLE MOVILES (
    Marca VARCHAR(20),
    Modelo VARCHAR(30),
    Descripcion VARCHAR(300),
    SO ENUM('iOS','Android'),
    RAM TINYINT UNSIGNED,
    PulgadasPantalla DECIMAL(3,2) UNSIGNED COMMENT 'Unidades en Pulgadas',
    CamaraMPX DECIMAL(2,2) UNSIGNED,
    CONSTRAINT FK_MOVILES PRIMARY KEY(Marca, Modelo)
);
DESC MOVILES;
/*
CREATE TABLE MOVIL_LIBRE(
    Marca_MOVILES VARCHAR(30),
    Modelo_MOVILES VARCHAR(30),
    Precio FLOAT,
    FOREIGN KEY(Marca_MOVILES, Modelo_MOVILES) REFERENCES MOVILES(Marca, Modelo)
);
DESC MOVIL_LIBRE;
*/
CREATE TABLE MOVIL_CONTRATO (
    Marca_MOVILES VARCHAR(20),
    Modelo_MOVILES VARCHAR(30),
    Nombre_OPERADORAS VARCHAR(20),
    Precio DECIMAL(6,2),
    CONSTRAINT FK_CONTRATO_MOVILES FOREIGN KEY(Marca_MOVILES, Modelo_MOVILES) REFERENCES MOVILES(Marca, Modelo),
    CONSTRAINT FK_CONTRATO_OPERADORAS FOREIGN KEY(Nombre_OPERADORAS) REFERENCES OPERADORAS(Nombre)
);
DESC MOVIL_CONTRATO;

CREATE TABLE OFERTAS(
    Nombre_OPERADORAS_TARIFAS VARCHAR(20),
    Nombre_TARIFAS VARCHAR(50),
    Marca_MOVIL_CONTRATO VARCHAR(20),
    Modelo_MOVIL_CONTRATO VARCHAR(30)
    CONSTRAINT FK_OFERTAS
);
DESC OFERTAS;


/*------------------------------------*/
/*  INSERCIÓN DE DATOS EN LAS TABLAS  */
/*------------------------------------*/

INSERT INTO CLIENTES VALUES
   ('12345678A','Perico','de los Palotes','123456789','perico@todo.com'),
   ('11111111Z','Fulanito','de Tal','666666999','ful@nito.com')
;
SELECT * FROM CLIENTES;

INSERT INTO TIENDAS VALUES
   ('Gran Vía','Madrid','Madrid','C/Gran Vía 32','141414142','Lunes','Viernes',9,18),
   ('Castellana','Madrid','Madrid','C/Castellana 108','432123321','Lunes','Sabado',8,15)
;
SELECT * FROM TIENDAS;

INSERT INTO OPERADORAS VALUES
   ('Vodafone','Rojo',99,540,'www.vodafone.com'),
   ('Movistar','Azul',98,610,'www.movistar.com')
;
SELECT * FROM OPERADORAS;

INSERT INTO TARIFAS VALUES
   ('Koala','Vodafone',4,'GB',200,'Si'),
   ('Ardilla','Vodafone',3,'GB',300,'Si')
;
SELECT * FROM TARIFAS;














