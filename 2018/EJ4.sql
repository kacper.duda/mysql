DROP DATABASE IF EXISTS EJ4;

CREATE DATABASE EJ4;

USE EJ4;

CREATE TABLE ESCENARIO(
	numero SMALLINT,
	riesgo VARCHAR(30),
	tiempo TIME,
	PRIMARY KEY(numero)
);
DESC ESCENARIO;

CREATE TABLE PERSONAJE(
	nombre VARCHAR(30),
	fuerza SMALLINT,
	inteligencia SMALLINT,
	habilidad SMALLINT,
	numeroESCENARIO SMALLINT,
	PRIMARY KEY(nombre,numeroESCENARIO),
	FOREIGN KEY(numeroESCENARIO) REFERENCES ESCENARIO(numero)
);
DESC PERSONAJE;

CREATE TABLE OBJETO(
	codigo INT(4) AUTO_INCREMENT UNIQUE,
	nombrePERSONAJE VARCHAR(30),
	numeroESCENARIO SMALLINT,
	horas SMALLINT,
	minutos SMALLINT,
	segundos SMALLINT,
	PRIMARY KEY(codigo,nombrePERSONAJE,numeroESCENARIO),
	FOREIGN KEY(nombrePERSONAJE) REFERENCES PERSONAJE(nombre),
	FOREIGN KEY(numeroESCENARIO) REFERENCES ESCENARIO(numero)
);
DESC OBJETO;
