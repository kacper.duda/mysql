/* *************** NO TE OLVIDES DE PONER EL WHERE EN EL DELETE FROM *************** */


/*------------------------------------*/
/*     CREACIÓN DE BASE DE DATOS      */
/*------------------------------------*/

DROP DATABASE IF EXISTS empresa;

CREATE DATABASE empresa;

USE empresa;

CREATE TABLE CLIENTES(
	ID INT UNSIGNED AUTO_INCREMENT,
	Nombre VARCHAR(30),
	Apellidos VARCHAR(40),
	DNI CHAR(9) NOT NULL,
	Telefono CHAR(9),
	Domicilio VARCHAR(80),

	CONSTRAINT PK_CLIENTES PRIMARY KEY(ID)
);
DESC CLIENTES;

CREATE TABLE PRODUCTOS(
	Codigo INT UNSIGNED AUTO_INCREMENT,
	Nombre VARCHAR(50),
	Precio DECIMAL(5,2),
	Cantidad INT,

	CONSTRAINT PK_PRODUCTOS PRIMARY KEY(Codigo)
);
DESC PRODUCTOS;

CREATE TABLE PEDIDOS(
	NumeroPedido INT UNSIGNED AUTO_INCREMENT,
	ID_CLIENTES INT UNSIGNED,
	Codigo_PRODUCTOS INT UNSIGNED,

	CONSTRAINT PK_PEDIDOS PRIMARY KEY(NumeroPedido),
	CONSTRAINT FK_PEDIDOS_CLIENTES FOREIGN KEY(ID_CLIENTES) REFERENCES CLIENTES(ID),
	CONSTRAINT FK_PEDIDOS_PRODUCTOS FOREIGN KEY(Codigo_PRODUCTOS) REFERENCES PRODUCTOS(Codigo)
);
DESC PEDIDOS;

/*------------------------------------*/
/*  INSERCIÓN DE DATOS EN LAS TABLAS  */
/*------------------------------------*/

INSERT INTO CLIENTES(Nombre,Apellidos,DNI,Telefono,Domicilio) VALUES 
	('Juan','Pérez','83922453J','694193461','C/ Gran Vía 3'),
	('Jose','Luis','3052453B','836492738','C/ Gran Vía 15'),
	('Pepe','López','85749372K','182364932','C/ Paseo de la Castellana 3');


/* crear base de datos

TABLAS: 
	clientes
	productos == cajero
	pedidos

generar usuario
	comercial - permisos para añadir clientes, modificar clientes, ver clientes
	admin - quitar usuarios, añadir usuario, modificar usuarios
	usuario - 
	gerente - 
	cajero -

crear 3 niveles de acceso - 
	Global
	DB
	Tablas
	Columnas
*/

/*------------------------------------*/
/*        Comandos de Usuarios        */
/*------------------------------------*/
/*

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin123';
GRANT ALL ON empresa.* TO 'admin'@'localhost';

CREATE USER 'gerente'@'localhost' IDENTIFIED BY 'gerente123';
GRANT UPDATE,SELECT,DELETE,INSERT ON empresa.* TO 'gerente'@'localhost';

CREATE USER 'comercial'@'localhost' IDENTIFIED BY 'comercial123';
GRANT 

CREATE USER 'usuario'@'localhost' IDENTIFIED BY 'usuario123';


*/












