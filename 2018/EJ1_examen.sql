/*------------------------------------*/
/*     CREACIÓN DE BASE DE DATOS      */
/*------------------------------------*/

DROP DATABASE IF EXISTS walter;

CREATE DATABASE walter;

USE walter;

CREATE TABLE PERSONA(
    nombre VARCHAR(20),
    apellidos VARCHAR(40),
    trabajo VARCHAR(30),
    PRIMARY KEY(nombre,apellidos)
);
DESC PERSONA;

CREATE TABLE OBJETO(
    nombre VARCHAR(30),
    tamaño ENUM('pequeño','mediano','grande'),
    nombreOBJETOcontenedor VARCHAR(20),
    PRIMARY KEY(nombre),
    FOREIGN KEY(nombreOBJETOcontenedor) REFERENCES OBJETO(Nombre)
);
DESC OBJETO;

CREATE TABLE SITUACION(
    hora TINYINT UNSIGNED,
    lugar VARCHAR(30),
    nombrePERSONA VARCHAR(20),
    apellidosPERSONA VARCHAR(40),
    vestuario VARCHAR(30),
    mercancia VARCHAR(30),
    PRIMARY KEY(hora,nombrePERSONA,apellidosPERSONA),
    FOREIGN KEY(nombrePERSONA,apellidosPERSONA) REFERENCES PERSONA(nombre,apellidos)
);
DESC SITUACION;

CREATE TABLE lleva(
    horaSITUACION TINYINT UNSIGNED,
    nombreOBJETO VARCHAR(30),
    FOREIGN KEY(horaSITUACION) REFERENCES SITUACION(hora),
    FOREIGN KEY(nombreOBJETO) REFERENCES OBJETO(nombre)
);
DESC lleva;

/*------------------------------------*/
/*  INSERCIÓN DE DATOS EN LAS TABLAS  */
/*------------------------------------*/

INSERT INTO PERSONA VALUES
    ('Walter','White','Profesor'),
    ('Jessie','Pinkman','Dealer'),
    ('Saul','Goodman','Abogado')
;
SELECT * FROM PERSONA;

INSERT INTO OBJETO VALUES
    ('Coche','grande',NULL),
    ('Mochila','mediano','Coche'),
    ('Pistola','pequeño','Mochila')
;
SELECT * FROM OBJETO;

INSERT INTO SITUACION VALUES
    (9,'Caravana','Jessie','Pinkman','Heisenberg','Baby Blue'),
    (10,'Despacho','Saul','Goodman','Traje','Pasta $$$')
;
SELECT * FROM SITUACION;

INSERT INTO lleva VALUES
    (9,'Pistola'),
    (10,'Mochila')
;
SELECT * FROM lleva;

