-----------------------------------------------------
|	    CONSULTAS BASE DE DATOS JARDINERIA	    |
-----------------------------------------------------

/* SACAR EL CÓDIGO DE OFICINA Y LA CIUDAD DONDE HAY OFICINAS */
	SELECT CodigoOficina, Ciudad FROM Oficinas; 

/* SACAR CUANTOS EMPLEADOS HAY EN LA COMPAÑIA */
	SELECT COUNT(CodigoEmpleado) FROM Empleados;

/* SACAR CUANTOS CLIENTES TIENE CADA PAIS */
	SELECT Pais, COUNT(*) AS 'Total Usuarios' FROM Clientes GROUP BY Pais;

/* SACAR CUAL FUE EL PAGO MEDIO EN 2009 */
	SELECT CONCAT(AVG(Cantidad),' €') AS 'Pago Medio 2009' FROM Pagos WHERE YEAR(FechaPago)=2009;

/* SACAR EL PRECIO TOTAL DE CADA PEDIDO */
	SELECT CodigoPedido, SUM(Cantidad * PrecioUnidad) AS 'PrecioTotal' FROM DetallePedidos GROUP BY CodigoPedido;

/* SACAR EL PEDIDO MÁS CARO */
	SELECT CodigoPedido, SUM(Cantidad * PrecioUnidad) AS 'PrecioTotal' FROM DetallePedidos GROUP BY CodigoPedido ORDER BY PrecioTotal DESC LIMIT 1;

/* SACAR EL CÓDIGO DEL CLIENTE QUE HA HECHO EL PEDIDO MÁS CARO */


/* SACAR LA CIUDAD Y EL TELÉFONO DE ESTADOS UNIDOS */
	SELECT Ciudad,Telefono FROM Oficinas WHERE Pais='EEUU';	

/* SACAR EL NOMBRE, APELLIDOS Y EMAIL DE LOS EMPLEADOS A CARGO DE ALBERTO SORIA */
	SELECT CodigoEmpleado,Nombre,CONCAT(Apellido1,' ',Apellido2),Email,CodigoJefe FROM Empleados WHERE CodigoJefe=3;

/* SACAR EL CARGO, NOMBRE, APELLIDOS Y EMAIL DEL JEFE DE LA EMPRESA */
	SELECT Puesto,Nombre,CONCAT(Apellido1,' ',Apellido2) AS Apellidos,Email FROM Empleados WHERE CodigoJefe IS NULL;

/* SACAR EL NOMBRE, APELLIDOS Y CARGO DE AQUELLOS QUE NO SEAN REPRESENTANTES DE VENTAS */
	SELECT Nombre,CONCAT(Apellido1,' ',Apellido2),Puesto FROM Empleados WHERE Puesto!='Representante Ventas';

/* SACAR EL NUMERO DE CLIENTES QUE TIENE LA EMPRESA */
	SELECT COUNT(CodigoCliente) AS 'Total Clientes' FROM Clientes;

/* SACAR LOS CLIENTES QUE NO HAYAN HECHO NINGÚN PEDIDO */
	SELECT Clientes.CodigoCliente,Clientes.NombreCliente FROM Clientes LEFT JOIN Pedidos ON Clientes.CodigoCliente=Pedidos.CodigoCliente WHERE Pedidos.CodigoCliente IS NULL ORDER BY Clientes.CodigoCliente ASC;

/* SACAR EL PAGO MEDIO DE TODOS LOS AÑOS */	
	SELECT YEAR(FechaPago),AVG(Cantidad) FROM Pagos GROUP BY YEAR(FechaPago);

/* SACAR LOS PAÍSES QUE TIENEN MAS DE 3 CLIENTES */
	SELECT Pais,COUNT(*) FROM Clientes GROUP BY Pais HAVING COUNT(*)>3;

/* MOSTRAR LOS PRODUCTOS QUE LA MEDIA ESTÉ POR ENCIMA DEL PRECIO */
	OPCION 1(cutre): 
		SELECT CodigoProducto,PrecioVenta FROM Productos WHERE PrecioVenta>22.59;
	OPCION 2: 
		SELECT CodigoProducto,PrecioVenta FROM Productos WHERE PrecioVenta>(SELECT AVG(PrecioVenta) FROM Productos);

/* SACAR EL NOMBRE DEL PRODUCTO MÁS CARO */		
	SELECT PrecioVenta,Nombre FROM Productos WHERE PrecioVenta=(SELECT MAX(PrecioVenta) FROM Productos);

/* SACAR EL PAGO TOTAL DE CADA PEDIDO Y MOSTRAR EL NOMBRE DEL CLIENTE */
	SELECT Clientes.NombreCliente,Pedidos.CodigoPedido,SUM(DetallePedidos.PrecioUnidad * DetallePedidos.Cantidad) AS PagoTotal FROM Clientes,Pedidos,DetallePedidos WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente AND Pedidos.CodigoPedido=DetallePedidos.CodigoPedido GROUP BY Pedidos.CodigoPedido ORDER BY Pedidos.CodigoPedido;

/* SACAR TOTAL DE PAGOS ENTRE TODOS LOS PEDIDOS DE UN CLIENTE */
	SELECT Clientes.CodigoCliente,Clientes.NombreCliente,SUM(DetallePedidos.PrecioUnidad * DetallePedidos.Cantidad) AS PagoTotal FROM Clientes,Pedidos,DetallePedidos WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente AND Pedidos.CodigoPedido=DetallePedidos.CodigoPedido GROUP BY Pedidos.CodigoCliente;

/* OBTENER EL NOMBRE DEL CLIENTE CON MAYOR LÍMITE DE CRÉDITO */
	SELECT Clientes.CodigoCliente,Clientes.NombreCliente FROM Clientes WHERE Clientes.LimiteCredito=(SELECT MAX(Clientes.LimiteCredito) FROM Clientes);

/* MOSTRAR TODOS LOS PEDIDOS QUE HECHO CADA CLIENTE */
	SELECT Clientes.NombreCliente,Clientes.CodigoCliente,Pedidos.CodigoPedido FROM Clientes,Pedidos WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente;
	SELECT Clientes.NombreCliente,Clientes.CodigoCliente,Pedidos.CodigoPedido FROM Clientes JOIN Pedidos ON Clientes.CodigoCliente=Pedidos.CodigoCliente; /* CON JOIN */

/* SACAR EL NOMBRE DEL EMPLEADO, CODIGO EMPLEADO, CODIGO DE LA OFICINA Y LA CIUDAD DE LA OFICINA */
	SELECT Empleados.Nombre,Empleados.CodigoEmpleado,Empleados.CodigoOficina,Oficinas.Ciudad FROM Empleados NATURAL JOIN Oficinas;

/* SACAR EL NOMBRE DEL CLIENTE, CODIGO CLIENTE, NOMBRE EMPLEADO Y CODIGO DEL EMPLEADO */
	SELECT Clientes.NombreCliente,Clientes.CodigoCliente,Empleados.Nombre,Empleados.CodigoEmpleado FROM Clientes JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;
	
/* MOSTAR EL CODIGO CLIENTE Y NOMBRE DEL CLIENTE, TENGAN O NO REPRESENTANTE DE VENTAS CON EL NOMBRE DEL EMPLEADO Y EL CODIGO DEL EMPLEADO */
	SELECT Clientes.CodigoCliente,Clientes.NombreCliente,Empleados.CodigoEmpleado,Empleados.Nombre FROM Clientes LEFT OUTER JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;

/* LA ANTERIOR AL REVES, MOSTRANDO TODOS LOS EMPLEADOS QUE NO TENGA CLIENTE */
	SELECT Clientes.CodigoCliente,Clientes.NombreCliente,Empleados.CodigoEmpleado,Empleados.Nombre FROM Clientes RIGHT OUTER JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;

/* MOSTRAR POR CADA EMPLEADO CUANTOS CLIENTES REPRESENTAN */
	SELECT Empleados.CodigoEmpleado,Empleados.Nombre,COUNT(Clientes.CodigoEmpleadoRepVentas) AS 'Total Clientes' FROM Clientes RIGHT JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado GROUP BY Empleados.CodigoEmpleado;

/* OBTENER EL NOMBRE,APELLIDO1 y CARGO QUE NO REPRESENTAN A NINGÚN CLIENTE */
	SELECT Empleados.Nombre,Empleados.Apellido1,Empleados.Puesto FROM Empleados LEFT JOIN Clientes ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Clientes.CodigoEmpleadoRepVentas IS NULL;

/* SACAR UN LISTADO CON EL NOMBRE CADA CLIENTE Y EL NOMBRE Y APELLIDO DE SU REPRESENTANTE DE VENTAS */
	SELECT Clientes.NombreCliente,Empleados.Nombre,Empleados.Apellido1 FROM Clientes JOIN Empleados ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas;

/* MOSTRAR EL CODIGO, NOMBRE Y APELLIDO DEL JEFE DE LOS EMPLEADOS */
	SELECT Curritos.CodigoEmpleado,Curritos.Nombre,Curritos.Apellido1,Curritos.CodigoJefe,Jefes.Nombre,Jefes.Apellido1 FROM Empleados AS Curritos LEFT JOIN Empleados AS Jefes ON Curritos.CodigoJefe=Jefes.CodigoEmpleado;

/* MOSTRAR CUANTOS SUBORDINADOS A TIENE A CARGO CADA EMPLEADO */
	SELECT Jefes.CodigoEmpleado,COUNT(*) FROM Empleados AS Curritos LEFT JOIN Empleados AS Jefes ON Curritos.CodigoJefe=Jefes.CodigoEmpleado GROUP BY Jefes.CodigoEmpleado;

/* MOSTRAR EL NOMBRE DE LOS CLIENTES QUE NO HAYAN REALIZADO PAGOS JUNTO CON EL NOMBRE DE SUS REPRESENTANTES DE VENTAS */
	OPCION 1: 
		SELECT Clientes.CodigoCliente,Clientes.NombreCliente,Empleados.CodigoEmpleado,Empleados.Nombre FROM Clientes JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Clientes.CodigoCliente IN(SELECT Clientes.CodigoCliente FROM Clientes NATURAL LEFT JOIN Pagos WHERE Pagos.CodigoCliente IS NULL ORDER BY Clientes.CodigoCliente);
	OPCION 2:
		SELECT Clientes.CodigoCliente,Clientes.NombreCliente,Empleados.CodigoEmpleado,Empleados.Nombre FROM Empleados JOIN Clientes NATURAL LEFT JOIN Pagos ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Pagos.CodigoCliente IS NULL ORDER BY Clientes.CodigoCliente;		 	 

/* LISTAR LAS VENTAS TOTALES DE LOS PRODUCTOS QUE HAYAN FACTURADO MÁS DE 3000 EUROS,SE MOSTRARÁ EL NOMBRE, UNIDADES VENDIDAS, TOTAL FACTURADO Y TOTAL FACTURADO CON IMPUESTOS(21% de IVA) */
	SELECT Productos.Nombre,DetallePedidos.CodigoProducto,SUM(DetallePedidos.Cantidad),SUM(DetallePedidos.Cantidad * DetallePedidos.PrecioUnidad),SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad)*1.21 FROM DetallePedidos NATURAL JOIN Productos GROUP BY DetallePedidos.CodigoProducto HAVING SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad)>3000;

/* MOSTRAR TODOS LOS CLIENTES CON NOMBRE DEL CLIENTE, NOMBRE DEL EMPLEADO, Nº PEDIDOS y CANTIDAD PAGADA */
/* SELECT Clientes.CodigoCliente,Clientes.NombreCliente,Empleados.Nombre,SUM(Pagos.Cantidad),COUNT(Pedidos.CodigoPedido) FROM Clientes LEFT JOIN (Empleados,Pagos,Pedidos) ON (Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado AND Clientes.CodigoCliente=Pagos.CodigoCliente AND Clientes.CodigoCliente=Pedidos.CodigoCliente) GROUP BY Clientes.CodigoCliente; */ ESTA MAL

/* LISTAR LA DIRECCIÓN DE LAS OFICINAS QUE TENGAN CLIENTES EN FUENLABRADA */
	SELECT DISTINCT Oficinas.LineaDireccion1 FROM Oficinas NATURAL LEFT JOIN Empleados LEFT JOIN Clientes ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Clientes.Ciudad='Fuenlabrada';
	SELECT Oficinas.CodigoOficina,Oficinas.LineaDireccion1,Clientes.Ciudad,Clientes.CodigoCliente FROM Oficinas LEFT JOIN Clientes ON Clientes.Region=Oficinas.Region WHERE Clientes.Ciudad='Fuenlabrada';

/* OBTENER EL PRODUCTO MÁS CARO */
	SELECT Productos.Nombre,Productos.CodigoProducto FROM Productos WHERE Productos.PrecioVenta=(SELECT MAX(Productos.PrecioVenta) FROM Productos);

/* OBTENER EL NOMBRE DEL PRODUCTO DEL QUE MÁS UNIDADES SE HAYAN VENDIDO EN UN MISMO PEDIDO */
	SELECT DetallePedidos.CodigoProducto,Productos.Nombre,DetallePedidos.Cantidad FROM DetallePedidos NATURAL JOIN Productos WHERE DetallePedidos.Cantidad=(SELECT MAX(DetallePedidos.Cantidad) FROM DetallePedidos);

/* OBTENER LOS CLIENTES CUYA LÍNEA DE CRÉDITO SEA MAYOR QUE LOS PAGOS QUE HAYA REALIZADO */
	SELECT Clientes.CodigoCliente,Clientes.LimiteCredito,Pagos.Cantidad FROM Clientes NATURAL JOIN Pagos;

/* SACAR EL PRODUCTO QUE MÁS UNIDADES TIENE EN STOCK Y EL QUE MENOS UNIDADES TIENE EN STOCK */		// /* || equivale a OR */ //
	SELECT Productos.CodigoProducto,Productos.Nombre,Productos.CantidadEnStock FROM Productos WHERE Productos.CantidadEnStock=(SELECT MAX(Productos.CantidadEnStock) FROM Productos) || Productos.CantidadEnStock=(SELECT MIN(Productos.CantidadEnStock) FROM Productos); 

/* SACAR EL NOMBRE DE LOS CLIENTES Y EL NOMBRE DE SUS REPRESENTANTES JUNTO CON LA CIUDAD DE LA OFICINA A LA QUE PERTENECE EL REPRESENTANTE */
	SELECT Clientes.NombreCliente,Empleados.Nombre,Oficinas.Ciudad FROM Clientes LEFT JOIN Empleados ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas LEFT JOIN Oficinas ON Empleados.CodigoOficina=Oficinas.CodigoOficina;

/* SACAR LA MISMA INFORMACIÓN QUE LA PREGUNTA ANTERIOR PERO SOLO LOS CLIENTES QUE NO HAYAN HECHO PAGOS */
	SELECT Clientes.CodigoCliente,Clientes.NombreCliente,Empleados.Nombre,Oficinas.Ciudad FROM Clientes LEFT JOIN Empleados ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas LEFT JOIN Oficinas ON Empleados.CodigoOficina=Oficinas.CodigoOficina LEFT JOIN Pagos ON Clientes.CodigoCliente=Pagos.CodigoCliente WHERE Pagos.CodigoCliente IS NULL;

/* OBTENER UN LISTADO CON EL NOMBRE DE LOS EMPLEADOS JUNTO CON EL NOMBRE DE SUS JEFES */
	SELECT Empleados.Nombre,Jefes.Nombre FROM Empleados LEFT JOIN Empleados AS Jefes ON Empleados.CodigoJefe=Jefes.CodigoEmpleado;

/* OBTENER EL NOMBRE DE LOS CLIENTES A LOS QUE NO SE LES HA ENTREGADO A TIEMPO UN PEDIDO */
	SELECT Clientes.NombreCliente FROM Clientes WHERE Clientes.CodigoCliente IN (SELECT DISTINCT CodigoCliente FROM Pedidos WHERE FechaEntrega <= FechaEsperada);

/* SACAR UN LISTADO DE CLIENTES INDICANDO EL NOMBRE DEL CLIENTE Y CUANTOS PEDIDOS HA REALIZADO */
	SELECT Clientes.NombreCliente,COUNT(*) FROM Clientes LEFT JOIN Pedidos ON Clientes.CodigoCliente=Pedidos.CodigoCliente WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente GROUP BY Clientes.CodigoCliente;

/* SACAR UN LISTADO DE LOS NOMBRE DE LOS CLIENTES Y EL TOTAL PAGADO POR CADA UNO DE ELLOS */ 
	SELECT Clientes.NombreCliente,SUM(Pagos.Cantidad) FROM Clientes NATURAL JOIN Pagos GROUP BY Pagos.CodigoCliente; 

/* SACAR EL NOMBRE DE LOS CLIENTES QUE HAYAN HECHO PEDIDOS EN 2008 */
	SELECT Clientes.NombreCliente,Pedidos.FechaPedido FROM Clientes NATURAL JOIN Pedidos WHERE Pedidos.FechaPedido LIKE '2008';

/* LISTAR EL NOMBRE DEL CLIENTE Y EL NOMBRE Y APELLIDO DE SUS REPRESENTANTES DE AQUELLOS CLIENTES QUE NO HAYAN REALIZADO PAGOS */
	SELECT Clientes.CodigoCliente,Clientes.NombreCliente,Empleados.Nombre,Empleados.Apellido1 FROM Clientes LEFT JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado LEFT JOIN Pagos ON Clientes.CodigoCliente=Pagos.CodigoCliente WHERE Pagos.CodigoCliente IS NULL;

/* SACAR UN LISTADO DE CLIENTES DONDE APAREZCA EL NOMBRE DE SU COMERCIAL Y LA CIUDAD DONDE ESTA SU OFICINA */
	SELECT Clientes.NombreCliente,Empleados.Nombre,Oficinas.Ciudad FROM Clientes LEFT JOIN Empleados ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas LEFT JOIN Oficinas ON Empleados.CodigoOficina=Oficinas.CodigoOficina;

/* SACAR EL NOMBRE,APELLIDOS,OFICINA Y CARGO DE AQUELLOS QUE NO SEAN REPRESENTANTES DE VENTAS */


/* BORRA LOS CLIENTES QUE NO TENGAN PEDIDOS */
	START TRANSACTION;
	DELETE FROM Clientes WHERE CodigoCliente NOT IN (SELECT DISTINCT CodigoCliente FROM Pedidos);
	ROLLBACK;

/* INCREMENTA EN UN 20% EL PRECIO DE LOS PRODUCTOS QUE NO TENGAN PEDIDOS */
	

/* BORRA LOS PAGOS DEL CLIENTE CON MENOR LÍMITE DE CRÉDITO */
	

/* ESTABLECE A 0 EL LÍMITE DE CRÉDITO DEL CLIENTE QUE MENOS UNIDADES PEDIDAS TENGA EL PRODUCTO OR-179 */
	



