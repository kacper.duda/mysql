/* *************** NO TE OLVIDES DE PONER EL WHERE EN EL DELETE FROM *************** */

/*------------------------------------*/
/*     CREACIÓN DE BASE DE DATOS      */
/*------------------------------------*/

DROP DATABASE IF EXISTS StarWars;

CREATE DATABASE StarWars;

USE StarWars;

CREATE TABLE ACTORES(
	Codigo INT AUTO_INCREMENT,
	Nombre VARCHAR(30),
	Fecha DATE,
	Nacionalidad VARCHAR(30) DEFAULT 'Estadounidense',
	
	CONSTRAINT PK_ACTORES PRIMARY KEY(Codigo)
);
DESC ACTORES;

CREATE TABLE PERSONAJES(
	Codigo INT AUTO_INCREMENT,
	Nombre VARCHAR(30),
	Raza VARCHAR(30) DEFAULT 'Humano',
	Grado SMALLINT UNSIGNED,
	Codigo_ACTORES INT,
	CodigoSuperior_PERSONAJES INT,
	
	CONSTRAINT PK_PERSONAJES PRIMARY KEY(Codigo),
	CONSTRAINT FK_ACTORES_PERSONAJES FOREIGN KEY(Codigo_ACTORES) REFERENCES ACTORES(Codigo),
	CONSTRAINT FK_PERSONAJES_PERSONAJES FOREIGN KEY(CodigoSuperior_PERSONAJES) REFERENCES PERSONAJES(Codigo)
);
DESC PERSONAJES;


/*------------------------------------*/
/*  INSERCIÓN DE DATOS EN LAS TABLAS  */
/*------------------------------------*/

INSERT INTO ACTORES(Nombre,Fecha) VALUES
	('Ewan McGregor','1971-03-31'),
	('Hayden Christensen','1981-04-19'),
	('Ian McDiarmid','1944-08-11')
;
SELECT * FROM ACTORES;

INSERT INTO PERSONAJES(Nombre,Grado,Codigo_ACTORES,CodigoSuperior_PERSONAJES) VALUES
	('ObiWan-KenoVe',7,1,NULL),
	('Lucas SkyWalker',9,2,1),
	('Emperador Palpame',10,3,NULL)
;
SELECT * FROM PERSONAJES;
