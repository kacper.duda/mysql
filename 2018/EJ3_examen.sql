/*------------------------------------*/
/*     CREACIÓN DE BASE DE DATOS      */
/*------------------------------------*/

DROP DATABASE IF EXISTS pnicolas;

CREATE DATABASE pnicolas;

USE pnicolas;

CREATE TABLE CONTACTO(
    ID INT AUTO_INCREMENT UNIQUE,
    nombre VARCHAR(20),
    apellidos VARCHAR(40),
    PRIMARY KEY(ID)
);
DESC CONTACTO;

CREATE TABLE REALEZA(
    ID_CONTACTO INT,
    ocupacion VARCHAR(30),
    PRIMARY KEY(ID_CONTACTO),
    FOREIGN KEY(ID_CONTACTO) REFERENCES CONTACTO(ID)
);
DESC REALEZA;

CREATE TABLE POLITICO(
    ID_CONTACTO INT,
    rango VARCHAR(30),
    partido VARCHAR(30),
    PRIMARY KEY(ID_CONTACTO),
    FOREIGN KEY(ID_CONTACTO) REFERENCES CONTACTO(ID)
);
DESC POLITICO;

CREATE TABLE FAMOSO(
    ID_CONTACTO INT,
    numero INT,
    apodo VARCHAR(30),
    PRIMARY KEY(ID_CONTACTO),
    FOREIGN KEY(ID_CONTACTO) REFERENCES CONTACTO(ID)
);
DESC FAMOSO;

CREATE TABLE EVENTO(
    lugar VARCHAR(30),
    fecha DATE,
    hora TIME,
    ID_CONTACTO_FAMOSO INT,
    cargo VARCHAR(30),
    PRIMARY KEY(lugar,fecha,hora,ID_CONTACTO_FAMOSO),
    FOREIGN KEY(ID_CONTACTO_FAMOSO) REFERENCES FAMOSO(ID_CONTACTO)
);
DESC EVENTO;

CREATE TABLE presenta(
    ID_CONTACTOpresentador INT,
    ID_CONTACTOpresentado INT,
    PRIMARY KEY(ID_CONTACTOpresentador,ID_CONTACTOpresentado),
    FOREIGN KEY(ID_CONTACTOpresentador) REFERENCES CONTACTO(ID),
    FOREIGN KEY(ID_CONTACTOpresentado) REFERENCES CONTACTO(ID)
);
DESC presenta;

/*------------------------------------*/
/*  INSERCIÓN DE DATOS EN LAS TABLAS  */
/*------------------------------------*/

INSERT INTO CONTACTO VALUES
	(NULL,'Juan','Antonio'),
	(NULL,'Pep','Antonio'),
	(NULL,'Jose','Luis'),
	(NULL,'Juan','Ruiz')
;
SELECT * FROM CONTACTO;

INSERT INTO REALEZA VALUES
	(3,'Jefe'),
	(4,'Rey'),
	(1,'Subjefe')
;
SELECT * FROM REALEZA;











