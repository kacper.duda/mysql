DROP DATABASE IF EXISTS EJ3;

CREATE DATABASE EJ3;

USE EJ3;

CREATE TABLE MUEBLE(
	Nombre VARCHAR(30),
	Precio DOUBLE(10,2),
	PRIMARY KEY(Nombre)
);
