/*------------------------------------------------------------------------------------------------*/
/*                    KACPER STANISLAW DUDA Y MIHAI GEORGE ALEXANDRU                              */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/*                                        R6-DATOS.SQL                                            */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------*/
/*         INSERCIÓN DE DATOS         */
/*------------------------------------*/
/*	 TEMPORADA;
	 OPERADOR;
	 DEFENSOR;
	 ATACANTE;
	 MAPA;
	 MODODEJUEGO;
	 ARMA;
	 EVENTO;
	 tiene;
	 asocia;
	 beneficia;
*/

INSERT INTO TEMPORADA (NumeroOperacion,Nombre,Informacion,Año) VALUES
	(4,'OPERATION RED CROW','Dos agentes nuevos y nuevo mapa',1),
	(2,'OPERATION HEALTH','Mejoras Técnicas',2),
	(3,'OPERATION GRIM SKY','Dos agentes nuevos expertos en ataque',3),
	(1,'OPERATION VELVET SHELL','Dos GEOS españoles',2),
	(5,'OPERATION SKULL RAIN','Dos nuevos agentes de la unidad brasileña',1)
;
SELECT * FROM TEMPORADA;
	
INSERT INTO OPERADOR (Apodo,NumeroOperacion_TEMPORADA,Nombre,Edad,Nacionalidad,Habilidad) VALUES
	('Valkyrie',2,'Megan J.Castellano',33,'California','Ojo Negro'),
	('Blackbeard',2,'Craig Jenson',34,'Washington','TARS Mk0'),
	('Caveira',3,'Taina Pereira',29,'Brasil','Paso Callado'),
	('Capitão',3,'Vicente Sousa',51,'Brasil','TAC Mk0'),
	('Clash',3,'Morowa Evans',35,'Inglatera','Escudo ADE'),
	('Bandit',1,'Dominic Brunsmeier',42,'Alemania','CED-1 Shock Wire'),
	('Lesion',3,'LIU TZE LONG',47,'JUNK BAY','GU MINE'),
	('Buck',1,'SEBASTIEN CÔTÉ',39,'Montreal','SK 4-12'),
	('Hibana',3,'YUMIKO IMAGAWA',36,'Japón','X-KAIROS'),
	('Glaz',1,'Timur Glazkov',30,'Rusia','HDS Flipsight')
;
SELECT * FROM OPERADOR;
	
INSERT INTO DEFENSOR (Apodo_OPERADOR,Identidad)	VALUES
	('Valkyrie',397235723),
	('Caveira',625142422),
	('Clash',323628943),
	('Bandit',874635453),
	('Lesion',456342356)
;
SELECT * FROM DEFENSOR;
	
INSERT INTO ATACANTE (Apodo_OPERADOR,Identidad) VALUES
	('Blackbeard',928736363),
	('Capitão',093864673),
	('Buck',853782833),
	('Hibana',123553321),
	('Glaz',435353532)
;
SELECT * FROM ATACANTE;
	
INSERT INTO MAPA (Nombre,NumeroOperacion_TEMPORADA,Pais) VALUES
	('Parque de Atracciones',4,'Hong Kong'),
	('Favela',2,'Brasil'),
	('Frontera',3,'Irak'),
	('El Rascacielos',1,'Japón'),
	('Litoral',5,'España')
;
SELECT * FROM MAPA;
	
INSERT INTO MODODEJUEGO (Nombre,Nombre_MAPA,Descripcion) VALUES
	('Entrenamiento','Parque de Atracciones','Rivales controlados por IA'),
	('Caza del terrorista','Favela','Solitario o Cooperativo'),
	('Rehén','Frontera','Localizar y extraer rehenes'),
	('Bomba','El Rascacielos','Localizar y neutralizar bombas'),
	('Asegurar el área','Litoral','Encontrar el material biológico')
;
SELECT * FROM MODODEJUEGO;	
	
INSERT INTO ARMA (ID,Nombre,tipo,Municion) VALUES
	(1,'MPX','Subfusil','.45'),
	(2,'SCAR-H','Fusil de Asalto','7,56x51'),
	(3,'M12','Subfusil','9 mm'),
	(4,'Para-308','Fusil de Asalto','7,56x51'),
	(5,'SPSMG9','Subfusil','9 mm')
;
SELECT * FROM ARMA;
	
INSERT INTO EVENTO (Nombre,Nombre_MAPA) VALUES
	('Hallowen','Parque de Atracciones'),
	('Outbreak','Favela'),
	('Mad House','Frontera'),
	('Pro League','El Rascacielos'),
	('Invitational','Litoral')
;
SELECT * FROM EVENTO;
	
INSERT INTO tiene (Nombre_MAPA,Nombre_MODODEJUEGO) VALUES
	('Parque de Atracciones','Entrenamiento'),
	('Favela','Caza del terrorista'),
	('Frontera','Rehén'),
	('El Rascacielos','Bomba'),
	('Litoral','Asegurar el área')
;
SELECT * FROM tiene;
	
INSERT INTO asocia (ID_ARMA,Apodo_OPERADOR,Cantidad) VALUES
	(1,'Valkyrie',1),
	(2,'Blackbeard',2),
	(3,'Caveira',1),
	(4,'Capitão',1),
	(5,'Clash',2)
;
SELECT * FROM asocia;
	
INSERT INTO beneficia (Apodo_OPERADOR_beneficiado,Apodo_OPERADOR_benefactor) VALUES
	('Valkyrie','Bandit'),
	('Caveira','Lesion'),
	('Blackbeard','Buck'),
	('Capitão','Glaz'),
	('Clash','Lesion'),
	('Buck','Hibana'),
	('Clash','Valkyrie'),
	('Lesion','Valkyrie')
;
SELECT * FROM beneficia;
	
/*------------------------------------*/
/*       ACTUALIZACIÓN DE DATOS       */
/*------------------------------------*/
UPDATE EVENTO SET Nombre='Verano' WHERE Nombre='Outbreak';
UPDATE ARMA SET Nombre='MP5' WHERE Nombre='M12';
UPDATE beneficia SET Apodo_OPERADOR_beneficiado='Clash' WHERE Apodo_OPERADOR_beneficiado='Bandit';
UPDATE beneficia SET Apodo_OPERADOR_beneficiado='Blackbeard' WHERE Apodo_OPERADOR_beneficiado='Hibana';

/*------------------------------------*/
/*          BORRADO DE DATOS          */
/*------------------------------------*/
DELETE FROM beneficia WHERE Apodo_OPERADOR_benefactor='Buck' AND Apodo_OPERADOR_beneficiado='Hibana';
DELETE FROM beneficia WHERE Apodo_OPERADOR_benefactor='Clash' AND Apodo_OPERADOR_beneficiado='Valkyrie';
DELETE FROM beneficia WHERE Apodo_OPERADOR_benefactor='Lesion' AND Apodo_OPERADOR_beneficiado='Valkyrie';

	



