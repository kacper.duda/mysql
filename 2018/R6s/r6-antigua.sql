/*------------------------------------------------------------------------------------------------*/
/*                    KACPER STANISLAW DUDA Y MIHAI GEORGE ALEXANDRU                              */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/*                                         R6-ANTIGUA.SQL                                         */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------*/
/*     CREACIÓN DE BASE DE DATOS      */
/*------------------------------------*/

DROP DATABASE IF EXISTS R6;

CREATE DATABASE R6;

USE R6;

/*------------------------------------*/
/*         CREACIÓN DE TABLAS         */
/*------------------------------------*/

/* TABLA TEMPORADA */
CREATE TABLE TEMPORADA (
	NumeroOperacion TINYINT UNIQUE,
	Nombre VARCHAR(50),
	Informacion TEXT,
	Año TINYINT,
	CONSTRAINT PK_TEMPORADA PRIMARY KEY(NumeroOperacion, Año)
);
DESC TEMPORADA;

/* TABLA OPERADOR */
CREATE TABLE OPERADOR (
	Apodo VARCHAR(40),
	NumeroOperacion_TEMPORADA TINYINT,
	Nombre VARCHAR(60),
	Edad SMALLINT,
	Nacionalidad VARCHAR(40),
	Habilidad VARCHAR(60),
	CONSTRAINT PK_OPERADOR PRIMARY KEY(Apodo,NumeroOperacion_TEMPORADA),
	CONSTRAINT FK_OPERADOR_TEMPORADA FOREIGN KEY(NumeroOperacion_TEMPORADA) REFERENCES TEMPORADA(NumeroOperacion)
);
DESC OPERADOR;

/* TABLA DEFENSOR */
CREATE TABLE DEFENSOR (
	Apodo_OPERADOR VARCHAR(40),
	RolDefensor VARCHAR(40),
	CONSTRAINT PK_DEFENSOR PRIMARY KEY(Apodo_OPERADOR,RolDefensor),
	CONSTRAINT FK_DEFENSOR_OPERADOR FOREIGN KEY(Apodo_OPERADOR) REFERENCES OPERADOR(Apodo)
);
DESC DEFENSOR;

/* TABLA ATACANTE */
CREATE TABLE ATACANTE (
	Apodo_OPERADOR VARCHAR(40),
	RolAtacante VARCHAR(40),
	CONSTRAINT PK_ATACANTE PRIMARY KEY(Apodo_OPERADOR,RolAtacante),
	CONSTRAINT FK_ATACANTE_OPERADOR FOREIGN KEY(Apodo_OPERADOR) REFERENCES OPERADOR(Apodo)
);
DESC ATACANTE;

/* TABLA MAPA */
CREATE TABLE MAPA (
	Nombre VARCHAR(40),
	NumeroOperacion_TEMPORADA TINYINT,
	Pais VARCHAR(40),
	CONSTRAINT PK_MAPA PRIMARY KEY(Nombre, NumeroOperacion_TEMPORADA),
	CONSTRAINT FK_MAPA_TEMPORADA FOREIGN KEY(NumeroOperacion_TEMPORADA) REFERENCES TEMPORADA(NumeroOperacion)	
);
DESC MAPA;

/* TABLA MODODEJUEGO */
CREATE TABLE MODODEJUEGO (
	Nombre VARCHAR(40),
	Nombre_MAPA VARCHAR(40),
	Descripcion TEXT,
	CONSTRAINT PK_MODODEJUEGO PRIMARY KEY(Nombre, Nombre_MAPA),
	CONSTRAINT FK_MODODEJUEGO_MAPA FOREIGN KEY(Nombre_MAPA) REFERENCES MAPA(Nombre)
);
DESC MODODEJUEGO;

/* TABLA ARMA */
CREATE TABLE ARMA (
	ID INT AUTO_INCREMENT UNIQUE,
	Nombre VARCHAR(40),
	Tipo VARCHAR(40),
	Municion VARCHAR(30),
	CONSTRAINT PK_ARMA PRIMARY KEY(ID)
);
DESC ARMA;

/* TABLA EVENTO */
CREATE TABLE EVENTO (
	Nombre VARCHAR(40),
	Nombre_MAPA VARCHAR(40),
	MapaExclusivo VARCHAR(40),
	ModoDeJuego VARCHAR(40),
	CONSTRAINT PK_EVENTO PRIMARY KEY(Nombre, Nombre_MAPA),
	CONSTRAINT FK_EVENTO_MAPA FOREIGN KEY(Nombre_MAPA) REFERENCES MAPA(Nombre)
);
DESC EVENTO;

/* TABLA tiene */
CREATE TABLE tiene (
	Nombre_MAPA VARCHAR(40),
	Nombre_MODODEJUEGO VARCHAR(40),
	CONSTRAINT PK_tiene PRIMARY KEY(Nombre_MAPA, Nombre_MODODEJUEGO),
	CONSTRAINT FK_tiene_MAPA FOREIGN KEY(Nombre_MAPA) REFERENCES MAPA(Nombre),
	CONSTRAINT FK_tiene_MODODEJUEGO FOREIGN KEY(Nombre_MODODEJUEGO) REFERENCES MODODEJUEGO(Nombre)
);
DESC tiene;

/* TABLA asocia */
CREATE TABLE asocia (
	ID_ARMA INT,
	Apodo_OPERADOR VARCHAR(40),
	Cantidad TINYINT,
	CONSTRAINT PK_asocia PRIMARY KEY(ID_ARMA, Apodo_OPERADOR),
	CONSTRAINT FK_asocia_ARMA FOREIGN KEY(ID_ARMA) REFERENCES ARMA(ID),
	CONSTRAINT FK_asocia_OPERADOR FOREIGN KEY(Apodo_OPERADOR) REFERENCES OPERADOR(Apodo)
);
DESC asocia;

/* TABLA beneficia */
CREATE TABLE beneficia (
	Apodo_OPERADOR_beneficiado VARCHAR(40),
	Apodo_OPERADOR_benefactor VARCHAR(40),
	CONSTRAINT PK_beneficia PRIMARY KEY(Apodo_OPERADOR_beneficiado, Apodo_OPERADOR_benefactor),
	CONSTRAINT FK_beneficia_OPERADOR_beneficiado FOREIGN KEY(Apodo_OPERADOR_beneficiado) REFERENCES OPERADOR(Apodo),
	CONSTRAINT FK_beneficia_OPERADOR_benefactor FOREIGN KEY(Apodo_OPERADOR_benefactor) REFERENCES OPERADOR(Apodo)
);
DESC beneficia;




