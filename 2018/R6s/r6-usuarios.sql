/*------------------------------------------------------------------------------------------------*/
/*                    KACPER STANISLAW DUDA Y MIHAI GEORGE ALEXANDRU                              */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------*/
/*                                         R6-USUARIOS.SQL                                        */
/*------------------------------------------------------------------------------------------------*/

/*------------------------------------*/
/*         BORRADO DE USUARIOS        */
/*------------------------------------*/

DROP USER IF EXISTS 'admin'@'localhost';
DROP USER IF EXISTS 'jefe_desarrollador'@'localhost';
DROP USER IF EXISTS 'desarrollador_general'@'localhost';
DROP USER IF EXISTS 'desarrollador_operador'@'localhost';
DROP USER IF EXISTS 'desarrollador_arma'@'localhost';
DROP USER IF EXISTS 'desarrollador_mapa'@'localhost';
DROP USER IF EXISTS 'anonimo'@'localhost';

/*------------------------------------*/
/*        CREACIÓN DE USUARIOS        */
/*------------------------------------*/

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admindesarrollador123'; /* Todos los permisos sobre las bases de datos (CREATE USER, DROP USER, RENAME USER, REVOKE ALL PRIVILEGES, CREATE VIEW) */
CREATE USER 'jefe_desarrollador'@'localhost' IDENTIFIED BY 'jefedesarrollador123'; /* Todos los permisos en tablas */
CREATE USER 'desarrollador_general'@'localhost' IDENTIFIED BY 'generaldesarrollador123'; /* Todas las tablas (UPDATE, DELETE, ALTER, CREATE TABLE, CREATE VIEW, SELECT) */
CREATE USER 'desarrollador_operador'@'localhost' IDENTIFIED BY 'operadordesarrollador123'; /* Tabla OPERADOR (UPDATE, DELETE, ALTER, SELECT) */
CREATE USER 'desarrollador_arma'@'localhost' IDENTIFIED BY 'armadesarrollador123'; /* Tabla ARMA (UPDATE, DELETE, ALTER, SELECT) */
CREATE USER 'desarrollador_mapa'@'localhost' IDENTIFIED BY 'mapadesarrollador123'; /* Tabla MAPA (UPDATE, DELETE, ALTER, SELECT) */
CREATE USER 'anonimo'@'localhost' IDENTIFIED BY 'anonimo'; /* SELECT en todas las tablas */

/*------------------------------------*/
/*        PERMISOS DE USUARIOS        */
/*------------------------------------*/

/* usuario admin */
GRANT ALL ON *.* TO 'admin'@'localhost';
GRANT CREATE USER,CREATE VIEW,GRANT OPTION ON *.* TO 'admin'@'localhost';

/* usuario jefe_desarrollador */
GRANT ALL ON R6.* TO 'jefe_desarrollador'@'localhost';
REVOKE DROP ON R6.* FROM 'jefe_desarrollador'@'localhost';

/* usuario desarrollador_general */
GRANT UPDATE,DELETE,ALTER,CREATE,CREATE VIEW,SELECT ON R6.* TO 'desarrollador_general'@'localhost';

/* usuario desarrollador_operador */
GRANT UPDATE,DELETE,ALTER,SELECT ON R6.OPERADOR TO 'desarrollador_operador'@'localhost';

/* usuario desarrollador_arma */
GRANT UPDATE,DELETE,ALTER,SELECT ON R6.ARMA TO 'desarrollador_arma'@'localhost';

/* usuario desarrollador_mapa */
GRANT UPDATE,DELETE,ALTER,SELECT ON R6.MAPA TO 'desarrollador_mapa'@'localhost';

/* usuario anonimo */
GRANT SELECT ON R6.* TO 'anonimo'@'localhost';


