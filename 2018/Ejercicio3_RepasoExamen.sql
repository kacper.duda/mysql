/* *************** NO TE OLVIDES DE PONER EL WHERE EN EL DELETE FROM *************** */


/*------------------------------------*/
/*     CREACIÓN DE BASE DE DATOS      */
/*------------------------------------*/

DROP DATABASE IF EXISTS EJ3_RepasoExamen;

CREATE DATABASE EJ3_RepasoExamen;

USE EJ3_RepasoExamen;

CREATE TABLE SOCIOS(
	Nombre VARCHAR(20) NOT NULL,
	Direccion VARCHAR(30),
	NumeroTelefono VARCHAR(15),
	FechaInscripcion DATE,
	NumeroSocio INT AUTO_INCREMENT,
	CONSTRAINT PK_SOCIOS PRIMARY KEY(NumeroSocio)
);
DESC SOCIOS;

CREATE TABLE LIBROS(
	Titulo VARCHAR(30) NOT NULL,
	Autor VARCHAR(20),
	FechaEditado DATE,
	NumeroLibro BIGINT AUTO_INCREMENT,
	CONSTRAINT PK_LIBROS PRIMARY KEY(NumeroLibro)
);
DESC LIBROS;

CREATE TABLE PRESTAMOS(
	FechaRetiro DATE,
	FechaEntrega DATE,
	NumeroSocio_SOCIOS INT,
	NumeroLibro_LIBROS BIGINT,
	IDPrestamo INT AUTO_INCREMENT,
	CONSTRAINT PK_PRESTAMOS PRIMARY KEY(IDPrestamo),
	CONSTRAINT FK_PRESTAMOS_SOCIOS FOREIGN KEY(NumeroSocio_SOCIOS) REFERENCES SOCIOS(NumeroSocio),
	CONSTRAINT FK_PRESTAMOS_LIBROS FOREIGN KEY(NumeroLibro_LIBROS) REFERENCES LIBROS(NumeroLibro)
);
DESC PRESTAMOS;

/*------------------------------------*/
/*  INSERCIÓN DE DATOS EN LAS TABLAS  */
/*------------------------------------*/

INSERT INTO SOCIOS(Nombre,Direccion,NumeroTelefono,FechaInscripcion) VALUES
	('PEDRO GIL','CANELONES','123.456.789','84-12-12'),
	('JOSE M. FORO','MONTEVIDEO','987.654.321','97-03-02'),
	('ELBA LAZO','LAS PIEDRAS','666.777.888','00-06-02'),
	('MARTA CANA','MONTEVIDEO','600.200.200','99-08-15')
;
SELECT NumeroSocio,Nombre,Direccion,NumeroTelefono,DATE_FORMAT(FechaInscripcion,"%d-%m-%y") FROM SOCIOS;

INSERT INTO LIBROS(Titulo,Autor,FechaEditado) VALUES 
	('EL METEOROLOGO','AITOR MENTA','54-12-12'),
	('LA FIESTA','ENCARNA VALES','87-03-02'),
	('EL GOLPE','MARCOS CORRO','94-12-25'),
	('LA FURIA','ELBIO LENTO','94-12-25')
;
SELECT NumeroLibro,Titulo,Autor,DATE_FORMAT(FechaEditado,"%d-%m-%y") FROM LIBROS;

INSERT INTO PRESTAMOS(FechaRetiro,FechaEntrega,NumeroSocio_SOCIOS,NumeroLibro_LIBROS) VALUES
	('03-12-12','03-12-22',1,1),
	('03-02-02','03-02-12',2,1),
	('03-02-02','03-02-12',3,2),
	('03-08-02','03-02-13',4,4)
;
SELECT IDPrestamo,NumeroSocio_SOCIOS,NumeroLibro_LIBROS,DATE_FORMAT(FechaRetiro,"%d-%m-%y"),DATE_FORMAT(FechaEntrega,"%d-%m-%y") FROM PRESTAMOS;



